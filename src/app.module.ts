/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Transport, ClientsModule } from '@nestjs/microservices';
import { MongooseModule } from '@nestjs/mongoose';
// import { JwtModule } from '@nestjs/jwt';
// import { Pay } from './dtos/entity/pay.dtos';
import { TransbankModule } from './transbank/transbank.module';

@Module({
  imports: [
    // ClientsModule.register([
    //   {
    //     name: 'PAY_SERVICE',
    //     transport: Transport.RMQ,
    //     options: {
    //       urls: ['amqp://rabbitmq:5672'], // sin docker es localhost
    //       queue: 'pay_queue',
    //       queueOptions: {
    //         durable: false,
    //       },
    //     },
    //   },
    // ]),
    // MongooseModule.forRoot(
    //   'mongodb+srv://gabrielZ:Password@cluster0.ob5kep8.mongodb.net/',
    // ),
    // JwtModule.register({
    //   secret: 'tu_clave_secreta', // Reemplaza con tu clave secreta real
    //   signOptions: { expiresIn: '1h' }, // Opciones de firma del token
    // }),
    TransbankModule
  ],
  controllers: [AppController],
  providers: [AppService, TransbankModule],
  exports: [AppService],
})
export class AppModule {}
