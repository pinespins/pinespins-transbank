import { Document } from 'mongoose';

export interface Pay extends Document {
  id: number;
  orden_compra: string;
  session_id: string;
  monto: number;
  url_retorno: string;
  
}