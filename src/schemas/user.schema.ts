import { Schema,Prop,SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type PayDocument = HydratedDocument<Pay>;

@Schema()
export class Pay{
  
  @Prop({required: true , unique: true})
  id: number

  @Prop({required: true})
  ordenCompra:string

  @Prop({required: true})
  sessionId:number
  
  @Prop({required: true})
  monto:number

  @Prop({})
  urlRetorno:string
}
export const PaySchema= SchemaFactory.createForClass(Pay)