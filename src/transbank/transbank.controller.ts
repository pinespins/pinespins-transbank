/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prettier/prettier */
import { Body, Controller, Post } from '@nestjs/common';
import { TransbankService } from './transbank.service';
import { MessagePattern, Payload } from '@nestjs/microservices';

@Controller('transbank')
export class TransbankController {
  constructor(private transbankService: TransbankService) {}

  @Post('CREATE')
  async create(
    @Body()
    payload: {
      sessionId: string;
      amount: number;
      protocol: string;
      host: string;
    },
  ) {
    console.log('[CREATE] payload: ', payload);
    return await this.transbankService.create(payload);
  }

  @Post('COMMIT')
  async commit(@Body() payload: { method: string; query: any; body: any }) {
    console.log('[COMMIT] payload: ', payload);
    const XD = await this.transbankService.commit(payload);
    console.log(XD)
    return XD;
  }
}
