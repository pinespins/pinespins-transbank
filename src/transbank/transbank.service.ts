/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
const WebpayPlus = require('transbank-sdk').WebpayPlus;

@Injectable()
export class TransbankService {
  create = async (request: {
    sessionId: string;
    amount: number;
    protocol: string;
    host: string;
  }) => {
    const buyOrder = 'O-' + Math.floor(Math.random() * 10000) + 1;
    const sessionId = request.sessionId
      ? request.sessionId
      : 'S-' + Math.floor(Math.random() * 10000) + 1;
    const amount = request.amount || Math.floor(Math.random() * 1000) + 1001;
    const returnUrl =
      request.protocol + '://' + request.host + '/payments/commit';

    const createResponse = await new WebpayPlus.Transaction().create(
      buyOrder,
      sessionId,
      amount,
      returnUrl,
    );

    const token = createResponse.token;
    const url = createResponse.url;

    const viewData = {
      buyOrder,
      sessionId,
      amount,
      returnUrl,
      token,
      url,
    };

    return viewData;
  };

  commit = async (request: any) => {
    //Flujos:
    //1. Flujo normal (OK): solo llega token_ws
    //2. Timeout (más de 10 minutos en el formulario de Transbank): llegan TBK_ID_SESION y TBK_ORDEN_COMPRA
    //3. Pago abortado (con botón anular compra en el formulario de Webpay): llegan TBK_TOKEN, TBK_ID_SESION, TBK_ORDEN_COMPRA
    //4. Caso atipico: llega todos token_ws, TBK_TOKEN, TBK_ID_SESION, TBK_ORDEN_COMPRA
    console.log(
      '================================================================================',
    );
    console.log(request);
    console.log(
      '================================================================================',
    );
    const params = request.payload.method === 'GET' ? request.payload.query : request.payload.body;

    const token = params.token_ws;
    const tbkToken = params.TBK_TOKEN;
    const tbkOrdenCompra = params.TBK_ORDEN_COMPRA;
    const tbkIdSesion = params.TBK_ID_SESION;

    let step = null;
    let stepDescription = null;
    const viewData = {
      token,
      tbkToken,
      tbkOrdenCompra,
      tbkIdSesion,
    };

    if (token && !tbkToken) {
      //Flujo 1
      const commitResponse = await new WebpayPlus.Transaction().commit(token);
      const viewData = {
        token,
        commitResponse,
      };

      return viewData;
    } else if (!token && !tbkToken) {
      //Flujo 2
      step = 'El pago fue anulado por tiempo de espera.';
      stepDescription =
        'En este paso luego de anulación por tiempo de espera (+10 minutos) no es necesario realizar la confirmación ';
    } else if (!token && tbkToken) {
      //Flujo 3
      step = 'El pago fue anulado por el usuario.';
      stepDescription =
        'En este paso luego de abandonar el formulario no es necesario realizar la confirmación ';
    } else if (token && tbkToken) {
      //Flujo 4
      step = 'El pago es inválido.';
      stepDescription =
        'En este paso luego de abandonar el formulario no es necesario realizar la confirmación ';
    }

    return ['Ocurrio un error!', viewData];
  };
}
